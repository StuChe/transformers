﻿using System;
using System.Collections.Generic;

namespace NewTransformers
{
    class Gun
    {
        public enum TheTypeOfGun
        {
            Меч = 1,
            Гранатомет,
            Базука,
            Огнемёт
        }
        public class KindOfGun
        {
            public TheTypeOfGun TypeOfGun;
            public Dictionary<string, int> GunDamage = new Dictionary<string, int>();
            public int damage;
            public void AddDamageOfGun()
            {
                GunDamage.Add("Меч", 10);
                GunDamage.Add("Гранатомет", 15);
                GunDamage.Add("Базука", 20);
                GunDamage.Add("Огнемёт", 5);
            }

        }
    }
}
